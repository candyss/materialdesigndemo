package com.cseenyen.materialdesigndemo;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.graphics.Palette;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by cseenyen on 16/03/15.
 */
public class MDDetailActivity extends Activity {
    private static final boolean USE_PALETTE = true;

    private LinearLayout mNameCountryLayout;
    private TextView mNameText;
    private TextView mCountryText;
    private ImageButton mButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.md_detail_layout);

        Bundle b = getIntent().getExtras();
        if(b == null) {
            Toast.makeText(this, "No object", Toast.LENGTH_SHORT).show();
            return;
        }

        DummyObject object = DummyObject.getObjectFromBundle(b);
        ImageView image = (ImageView) findViewById(R.id.big_image);
        image.setImageDrawable(getResources().getDrawable(object.getImageRes()));
        TextView desc = (TextView) findViewById(R.id.desc);
        desc.setText(object.getDescription());

        ViewCompat.setTransitionName(image, "image");

        mNameCountryLayout = (LinearLayout) findViewById(R.id.name_country_layout);
        mNameText = (TextView) findViewById(R.id.place_name);
        mNameText.setText(object.getName());
        mCountryText = (TextView) findViewById(R.id.place_country);
        mCountryText.setText(object.getSubtitle());
        mButton = (ImageButton) findViewById(R.id.fab);

        Bitmap icon = BitmapFactory.decodeResource(getResources(), object.getImageRes());
        if(icon != null) {
            Palette p = Palette.generate(icon);
            setColors(p);
        }
    }

    private void setColors(Palette p) {
        mNameCountryLayout.setBackgroundColor(p.getVibrantColor(R.color.mdd_main));
        mNameText.setTextColor(p.getDarkVibrantColor(R.color.mdd_primary_text));
        mCountryText.setTextColor(p.getDarkMutedColor(R.color.mdd_text));
        mButton.setVisibility(View.VISIBLE);
        LayerDrawable layers = (LayerDrawable) mButton.getBackground();
        GradientDrawable shape = (GradientDrawable) (layers.findDrawableByLayerId(R.id.fab_bg_drawable));
        shape.setColor(p.getDarkVibrantColor(R.color.mdd_accent));
    }

    @Override
    public void onBackPressed() {
        ActivityCompat.finishAfterTransition(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id == android.R.id.home) {
            ActivityCompat.finishAfterTransition(this);
            return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
