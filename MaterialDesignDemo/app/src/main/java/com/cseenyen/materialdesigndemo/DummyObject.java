package com.cseenyen.materialdesigndemo;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cseenyen on 15/03/15.
 */
public class DummyObject {
    private String mName;
    private String mSubtitle;
    private String mDescription;
    private int mImageRes;
    private int mGridImageRes;

    public DummyObject(String name, String subtitle, String desc, int res, int gridRes) {
        mName = name;
        mSubtitle = subtitle;
        mDescription = desc;
        mImageRes = res;
        mGridImageRes = gridRes;
    }

    public String getName() {
        return mName;
    }

    public String getSubtitle() {
        return mSubtitle;
    }

    public String getDescription() {
        return mDescription;
    }

    public int getImageRes() {
        return mImageRes;
    }

    public int getGridImageRes() {
        return mGridImageRes;
    }

    public static List<DummyObject> getDummyObjects() {
        ArrayList<DummyObject> objects = new ArrayList<DummyObject>();
        objects.add(new DummyObject("Great Wall of China",
                "China",
                "The Great Wall of China is so grand in its scale that it snakes its way through the People’s Republic, in various tangents, for more than 20,000 kilometers (12,425 miles). As UNESCO notes, “its historic and strategic importance is matched only by its architectural significance.” Construction began around 220 B.C. under Qin Shi Huang and continued all the way up to the Ming Dynasty (1368-1644), reflecting the military might and political strength of the central empires in ancient China. Though you’ll have to cast your preconceived notions of a single wall aside, each of the many barricades that make up The Great Wall of China have their own stories to tell, and each offer a fascinating look at dynastic China.",
                R.drawable.great_wall,
                R.drawable.great_wall_grid));
        objects.add(new DummyObject("Machu Picchu",
                "Peru",
                "It has been some five centuries since the fall of the Inca Empire and one century since U.S. historian Hiram Bingham “rediscovered” the civilization's most famous citadel, Machu Picchu, and history has come full circle. The once bustling Inca estate, abandoned and forgotten, is now busier than it’s ever been, with as many as 2,500 visitors a day. The hike up to these pre-Columbian ruins (which lie at 2,430 meters, or nearly 8,000 feet above sea) is, quite literally, breathtaking. And what you’ll see from the top is a serene spot that’s frequently shrouded in an ethereal fog and perpetually blanketed in emerald green grass. It’s a place fit for a king, which of course is exactly why it was built in the first place.",
                R.drawable.machu_pichu,
                R.drawable.machu_pichu_grid));
        objects.add(new DummyObject("Grand Canyon",
                "USA",
                "It’s one of the seven wonders of the natural world, but it doesn’t need any titles to impress; the numbers speak for themselves. The formidable Grand Canyon is 446 river kilometers (227 miles) long, up to 29 kilometers (18 miles) wide and 1.6 kilometers (1 mile) deep, and its walls offer a striking mosaic of geological colors and erosional forms that could put any museum to shame -- particularly when the colors change at sunrise and sunset. Add to the mix a few curtain-like waterfalls, Native American ruins and curious desert-dwellers, and you’ve got yourself a postcard-perfect vacation.",
                R.drawable.gran_canyon,
                R.drawable.gran_canyon_grid));
        objects.add(new DummyObject("Pyramids of Giza",
                "Egypt",
                "Spent your whole life looking at images of Egypt’s Great Pyramids and figured them to be in the middle of the dessert? Their proximity to modern Cairo may shock you. But the somewhat urban setting does not take away from the reality that these massive limestone beacons, and their guard cat, the Great Sphinx, are more than 4,500 years old (by most estimates) and still standing as the only surviving wonders of the ancient world. For modern day time travel, it doesn’t get much better than this.",
                R.drawable.pyramids,
                R.drawable.pyramids_grid));
        objects.add(new DummyObject("Great Barrier Reef",
                "Australia",
                "Australia’s Great Barrier Reef is 2,600 kilometers (1,680 miles) in length and easily the largest living structure on earth. It’s also the only living thing visible from outer space -- though it’s best appreciated in person off the Queensland coast on one of the many dive trips or snorkel safaris. This massive underwater wonderland is composed of more than 2,900 individual reefs and 900 islands that house a whopping 1,500 fish species, 30 species of whales and dolphins, six species of sea turtles and more than 215 species of birds. If you’re looking for the New York or Tokyo of the ocean, this is it.",
                R.drawable.barrier_reef,
                R.drawable.barrier_reef_grid));
        objects.add(new DummyObject("The Colosseum",
                "Italy",
                "Though completed in 80 A.D., Rome’s Colosseum remains the world’s largest amphitheater nearly 2,000 years later, and a towering testament to the technological prowess of the Roman Empire. Its walls have seen both heroic and barbaric acts, including violent bouts of gladiatorial combat and thousands of slaughtered animals. The structure today, however, is known less for its bloody “sports” and more for its giddy, camera-toting tourists.",
                R.drawable.coloseum,
                R.drawable.coloseum_grid));
        objects.add(new DummyObject("The Sistine Chapel",
                "Vatican City",
                "Four years in the making, Michelangelo’s interpretation of some of the Old Testament’s most powerful stories -- three each from the creation, the fall of Adam and Eve, and the story of Noah -- has become one of the iconic sets of images in Western art. While it was certainly the talk of the town in 1512, 501 years later, the Sistine Chapel is so renown it attracts as many as 20,000 tourists each day, who line up to take a peek at the world’s most visited room.",
                R.drawable.sistine_chapel,
                R.drawable.sistine_chapel_grid));
        objects.add(new DummyObject("Bora Bora",
                "French Polynesia",
                "Bora Bora may as well be shorthand for paradise. It’s the Ferrari of exotic locales, dripping with luxury and dotted with glass-bottomed bungalows that boast endless vistas of aquamarine sea and jagged volcanic peaks. Of course, this is a dream that comes with a hefty price tag, but the location of this Society Island of French Polynesia, smack dab in the middle of the vast Pacific Ocean, ensures that you leave all your struggles and your cares thousands of miles away.",
                R.drawable.bora_bora,
                R.drawable.bora_bora_grid));
        objects.add(new DummyObject("Yellowstone Ntl. Park",
                "USA",
                "It was the wild plains of buffalo, bears, wolves and elk and the extraordinary natural art gallery of geysers, hot springs and scorched, bubbling earth that spurred U.S. President Ulysses S. Grant to create the world’s first national park in March 1872 and name it Yellowstone. Conservation efforts here would be replicated the world over, while the name Yellowstone itself would conjure up images of America’s Wild West. Today, it remains one of the most fabled spots on the U.S. map, and a Mecca for outdoor enthusiasts, who gush over its flora, fauna and geothermal unpredictability.",
                R.drawable.yellowstone,
                R.drawable.yellowstone_grid));
        objects.add(new DummyObject("The Eiffel Tower",
                "France",
                "When Gustave Eiffel announced that he would build the tallest tower in the world for the 1889 World’s Fair, he expected a euphoric response. What he got instead was skepticism, virulent criticism and even protests “on artistic grounds.”  History, however, would prove his critics to be a pack of fools. The Eiffel Tower is now not only the world’s most-visited paid monument, but also its most valuable at $561.9 billion, according to a 2012 report, making it a crucial cog in the French economy and one of the most beloved sites in Europe.",
                R.drawable.eiffel,
                R.drawable.eiffel_grid));
        return objects;
    }

    public static DummyObject getObjectFromBundle(Bundle b) {
        String name = b.getString("name");
        String subtitle = b.getString("subtitle");
        String desc = b.getString("desc");
        int imageRes = b.getInt("imageRes");
        int gridImageRes = b.getInt("gridImageRes");
        return new DummyObject(name, subtitle, desc, imageRes, gridImageRes);
    }
}
