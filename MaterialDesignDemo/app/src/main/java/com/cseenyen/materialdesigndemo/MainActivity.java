package com.cseenyen.materialdesigndemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


public class MainActivity extends Activity implements RecyclerAdapter.ItemClickListener{
    private static final boolean MATERIAL_DESIGN_VERSION = false;

    private RecyclerView mGrid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(MATERIAL_DESIGN_VERSION) {
            setContentView(R.layout.md_activity_main);
            findMaterialViews();
        } else {
            setContentView(R.layout.activity_main);
            findViews();
        }

        getActionBar().setTitle("Places to visit");
    }

    private void findViews() {
        ListView list = (ListView) findViewById(R.id.listview);
        final ListAdapter adapter = new ListAdapter(this, DummyObject.getDummyObjects());
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DummyObject object = adapter.getItem(position);
                itemSelected(object);
            }
        });
    }

    private void findMaterialViews() {
        mGrid = (RecyclerView) findViewById(R.id.grid);
        mGrid.setHasFixedSize(true); // allows optimizations knowing that the size of the adapter content will not change
        mGrid.setLayoutManager(new GridLayoutManager(this, 2));
        RecyclerAdapter adapter = new RecyclerAdapter(this, this, DummyObject.getDummyObjects());
        mGrid.setAdapter(adapter);
    }

    @Override
    public void itemClicked(DummyObject item) {
        itemSelected(item);
    }

    private void itemSelected(DummyObject object) {
        Bundle b = new Bundle();
        b.putString("name", object.getName());
        b.putString("subtitle", object.getSubtitle());
        b.putString("desc", object.getDescription());
        b.putInt("imageRes", object.getImageRes());
        b.putInt("gridImageRes", object.getGridImageRes());
        Intent i;
        if(MATERIAL_DESIGN_VERSION) {
            i = new Intent(MainActivity.this, MDDetailActivity.class);
            i.putExtras(b);
            RecyclerAdapter.ViewHolder viewHolder = (RecyclerAdapter.ViewHolder) mGrid.findViewHolderForItemId(object.getImageRes());
            String image = "image";
            Pair<View, String> imagePair = Pair.create(viewHolder.getImageView(), image);
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, imagePair);
            ActivityCompat.startActivity(this, i, options.toBundle());
        } else {
            i = new Intent(MainActivity.this, DetailActivity.class);
            i.putExtras(b);
            startActivity(i);
        }
    }
}