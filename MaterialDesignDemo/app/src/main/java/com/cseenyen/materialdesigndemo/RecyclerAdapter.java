package com.cseenyen.materialdesigndemo;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by cseenyen on 16/03/15.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    public interface ItemClickListener {
        public void itemClicked(DummyObject item);
    }

    private Context mContext;
    private ItemClickListener mListener;
    private List<DummyObject> mItems;

    public RecyclerAdapter(Context context, @NonNull ItemClickListener listener, List<DummyObject> items) {
        mContext = context;
        mListener = listener;
        mItems = items;
        setHasStableIds(true);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public long getItemId(int position) {
        return mItems.get(position).getImageRes();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        View parent = LayoutInflater.from(context).inflate(R.layout.md_grid_item_layout, viewGroup, false);
        return ViewHolder.newInstance(parent);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final DummyObject item = mItems.get(position);
        holder.setImage(item.getGridImageRes());
        holder.setTitle(item.getName());
        holder.setSubtitle(item.getSubtitle());
        holder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.itemClicked(item);
            }
        });
    }

    public static final class ViewHolder extends RecyclerView.ViewHolder {
        private final View parent;
        private final ImageView image;
        private final TextView title;
        private final TextView subtitle;

        public static ViewHolder newInstance(View parent) {
            ImageView image = (ImageView) parent.findViewById(R.id.grid_image);
            TextView title = (TextView) parent.findViewById(R.id.grid_name);
            TextView description = (TextView) parent.findViewById(R.id.grid_subtitle);
            return new ViewHolder(parent, image, title, description);
        }

        private ViewHolder(View parent, ImageView image, TextView title, TextView description) {
            super(parent);
            this.parent = parent;
            this.image = image;
            this.title = title;
            this.subtitle = description;
        }

        public void setImage(int imageRes) {
            this.image.setImageResource(imageRes);
        }

        public void setTitle(CharSequence text) {
            title.setText(text);
        }

        public void setSubtitle(CharSequence text) {
            subtitle.setText(text);
        }

        public void setOnClickListener(View.OnClickListener listener) {
            parent.setOnClickListener(listener);
        }

        public View getImageView() {
            return this.image;
        }
    }
}
