package com.cseenyen.materialdesigndemo;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by cseenyen on 15/03/15.
 */
public class ListAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private List<DummyObject> mItems;

    public ListAdapter(Context context, List<DummyObject> items) {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        mItems = items;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public DummyObject getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.md_list_item_layout, parent, false);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.subtitle = (TextView) convertView.findViewById(R.id.subtitle);
            holder.image = (ImageView) convertView.findViewById(R.id.image);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        DummyObject item = getItem(position);
        holder.name.setText(item.getName());
        holder.subtitle.setText(item.getSubtitle());
        holder.image.setImageDrawable(mContext.getResources().getDrawable(item.getImageRes()));
        return convertView;
    }

    private class ViewHolder {
        TextView name;
        TextView subtitle;
        ImageView image;
    }
}
