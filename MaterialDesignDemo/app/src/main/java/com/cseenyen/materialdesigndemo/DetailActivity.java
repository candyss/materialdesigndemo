package com.cseenyen.materialdesigndemo;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by cseenyen on 15/03/15.
 */
public class DetailActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_layout);
        Bundle b = getIntent().getExtras();
        if(b == null) {
            Toast.makeText(this, "No object", Toast.LENGTH_SHORT).show();
            return;
        }

        DummyObject object = DummyObject.getObjectFromBundle(b);
        ImageView image = (ImageView) findViewById(R.id.big_image);
        image.setImageDrawable(getResources().getDrawable(object.getImageRes()));
        TextView desc = (TextView) findViewById(R.id.desc);
        desc.setText(object.getDescription());

        getActionBar().setTitle(object.getName());
    }
}
